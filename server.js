const http = require('http')
const fs = require('fs')
const url = require('url')
const express = require('express')
const bodyParser = require('body-parser')
const json = require('jsonfile')
const ipRangeCheck = require("ip-range-check")
const _ = require('lodash')
const git = require("nodegit")

// initialize the queue
let queue = []
let queuesize = 20
for (let i = 0 ; i < queuesize ; i++) {
  queue.push(null)
}

// the pull function
const pull = (repositoryPath, remoteName, branch, cb) => {
    var repository;
    var remoteBranch = remoteName + '/' + branch;
    git.Repository.open(repositoryPath)
        .then(function (_repository) {
            repository = _repository;
            return repository.fetch(remoteName);
        }, cb)
        .then(function () {
            return repository.mergeBranches(branch, remoteBranch);
        }, cb)
        .then(function (oid) {
            cb(null, oid);
        }, cb);
};

// open cfg file
const cfg = json.readFileSync(__dirname + "/cfg.json")

// create express app
const app = express()
// set body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
// start server
const server = http.Server(app)

app.post('*', (req, res) => {
  // get the event key & ip
  const eventkey = req.header('x-event-key')
  const ip = req.ip
  // check if ip is in whitelist
  if (!_.some(cfg.whitelist, (wip) => ipRangeCheck(ip, wip))) {
    res.end("IP (" + ip + ") not in whitelist")
    return
  }
  // check if push
  if (eventkey !== "repo:push") {
    res.end("Not a repo push hook")
    return
  }
  // check if already treated
  if (_.includes(queue, req.body.push.changes[0].new.target.hash)) {
    res.end("Push aleady treated !")
    return
  }
  // add in the queue
  queue.push(req.body.push.changes[0].new.target.hash)
  queue.shift()
  // get the first repo in cfg that matches
  const repoIdx = _.findIndex(cfg.repositories, (repo) => {
      // get uuid stored
      return req.body.repository.uuid === repo.uuid
  })
  // check if exists
  if (repoIdx < 0) {
    res.end("Repository not listed in cfg")
    return
  }

  // get repo
  const repository = cfg.repositories[repoIdx]
  // repo exists. open
  pull(repository.folder, repository.remote, repository.branch, (err, oid) => {
    if (err) {
      res.end("An error occured : " + err)
      console.log(err)
      return
    }
    res.end("OK!")
    console.log(oid)
    // post apply

    if (repository.postapply) {
      require(__dirname + repository.postapply.script)(repository, repository.postapply.params)
    }
  })
})

// handle 404
app.get('*', (req, res) => {
  // get url
  const urlin = url.parse(req.url)
  const page = urlin.pathname;
  const params = new url.URL('http://' + req.headers.host + req.url).searchParams
  // log
  console.log('ENOENT File "' + page + '" requested.')
  // write
  res.writeHead(404, {'Content-Type': 'text/plain'})
  res.end()
})


// listen
server.listen(cfg["port"])
// log
console.log('Server started on port %d', cfg["port"])
