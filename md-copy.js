const recursive = require("recursive-readdir")
const path = require('path')
const _ = require('lodash')
const json = require('jsonfile')
const fs = require('fs')
const ncp = require('ncp').ncp
const rmrf = require('rimraf')

ncp.limit = 16

module.exports = (repository, params) => {
  // the base path
  const pathfile = repository.folder
  const targetfolder = params.target + "_posts"
  const assetsfolder = params.target + "img"
  // open the mdignore : TODO
  // the ignore function
  const ignore = (file, stats) => false
  // read files
  recursive(pathfile, [ignore], function (err, files) {
    // if error, log
    if (err) {
      console.log(err)
      return
    }
    // prune
    const mds = _.map(_.filter(files, (file) => path.extname(file) === ".md"), (md) => path.relative(pathfile, md))
    const descfiles = {}
    // go through each md filename and separate
    const mdsmetadata = _.map(mds, (md) => {
      // split
      const splitdata = md.split(path.sep)
      const category = splitdata[0]
      const type = splitdata[1] || ""
      const typefancy = _.capitalize(splitdata[1] ? (splitdata[1] + " ") : "")
      const name = path.basename(splitdata[splitdata.length - 1], '.md')
      // get descfile
      if (!_.has(descfiles, category)) {
        try {
          descfiles[category] = json.readFileSync(path.resolve(pathfile, "./" + category) + "/.description")
        } catch(e) {
          console.log(".description missing for category " + category + ". Defaulting")
          descfiles[category] = {
            title: category
          }
        }
      }
      // compute fullname
      const fullname = descfiles[category].title + " - " + typefancy + name.replace(/(\d+)-(\d+)-(\d+)/, "$1/$2/20$3")
      const filename = name.replace(/(\d+)-(\d+)-(\d+)/, "20$3-$2-$1") + "-" + category + "-" + type + ".md"
      const permalink = "/" + category + "/" + (type ? (type + '/') : "") + name
      // return
      return {
        source: md,
        dest: filename,
        category: category,
        type: type,
        img: path.dirname(md) + "/img",
        header: '---\ncategory: ' + descfiles[category].title + '\nlayout: post\ntitle:  "' + fullname + '"\npermalink: ' + permalink + '\n---\n'
      }
    })

    // done files
    var imgfolders = []
    // remove image folder
    rmrf(assetsfolder, () => {
      // make img folder
      fs.mkdir(assetsfolder, (e3) => {
        // if error, throw
        if(err) {
          console.log(err)
          return
        }
        // delete all files
        recursive(targetfolder, [], (e, files) => {
          // throw if error
          if(e) {
            console.log(e)
            return
          }
          // remove each file
          Promise.all(_.map(files, (file) => {
              return new Promise((resolve, reject) => {
                fs.unlink(file, (e2) => {
                  if (e2) {
                    reject(e2)
                  }
                  resolve()
                })
              })
          }))
          .then(() => {
            // copy all files
            _.forEach(mdsmetadata, (md) => {
              // read the file
              fs.readFile(path.resolve(pathfile, md.source), 'utf8', (err, data) => {
                if(err) {
                  console.log(err)
                  return
                }
                // log
                console.log('done reading file')
                // replace in data to fix markdown parsing w/ mathjax
                data = data.replace(/\\\\/g, "\\\\\\\\\\\\\\\\")
                data = data.replace(/\$\$([^\$]*)\$\$/gm, "\\\\[$1\\\\]")
                data = data.replace(/\$([^\$]*)\$/g, "\\\\($1\\\\)")
                data = data.replace(/\\\{/g, "\\\\{")
                data = data.replace(/\\\}/g, "\\\\}")
                data = data.replace(/\u00A0/g, "\u0020")
                data = data.replace(/]\(img\//g, "](/img/" + md.category + (md.type ? "-" + md.type : "") + "-")
                // write
                fs.writeFile(path.resolve(targetfolder, md.dest), md.header + data, 'utf8', (err2, data) => {
                  if(err) {
                    console.log(err)
                    return
                  }
                  // log
                  console.log('done writing')
                  // check done img files
                  if (!_.includes(imgfolders, md.img)) {
                    recursive(path.resolve(pathfile, md.img), [], (e4, imgfiles) => {
                      // throw if error
                      if(e4) {
                        console.log(e4)
                        return
                      }
                      // get list of files in image folder
                      _.forEach(imgfiles, (imgfilename) => {
                        // compute new filename
                        const newimage = md.category + (md.type ? "-" + md.type : "") + "-" + path.basename(imgfilename)
                        // copy image
                        fs.createReadStream(imgfilename).pipe(fs.createWriteStream(path.resolve(assetsfolder, newimage)))
                        // done copying
                        console.log("done copying " + imgfilename)
                      })
                    })
                    // add to imgfolders
                    imgfolders.push(md.img)
                  }
                })
              })
            })
          })
          .catch(err => console.log(e))
        })
      })
    })
  })
}
