const recursive = require("recursive-readdir")
const path = require('path')
const _ = require('lodash')
const json = require('jsonfile')
const fs = require('fs')
const rmrf = require('rimraf')
const mysql = require('mysql')

module.exports = (repository, params) => {
  /* The base path */
  const basepath = repository.folder
  /* The courses */
  let subjects = {}
  let courses = {}
  /* Read all files */
  fs.readdir(basepath, (e, files) => {
    /* Check error */
    if (e) { console.log(e); return; }
    /* Get directories */
    let directories = []
    try {
      /* Lstat of all files */
      directories =
        files.filter(name => fs.lstatSync(path.join(basepath, name)).isDirectory())
    } catch(e) { console.log(e); return; }
    /* For each subject, read the json file  */
    for (var i = 0; i < directories.length; i++) {
      try {
        /* Read the json file */
        const descfile = json.readFileSync(path.join(basepath, directories[i], ".description"))
        /* Initialize subjects */
        subjects[directories[i]] = {
          title: descfile.title || "Untitled course",
          fulltitle: descfile.fulltitle || "Untitled course",
          duration: ~~descfile.duration,
          courseid: descfile.id || "?.?",
          shorttitle: directories[i],
          link: descfile.link || "#"
        }
      } catch(e) {
        /* If enoent, skip dir */
        if (!(e instanceof Error && e.code === "ENOENT")) {
          throw e;
        }
      }
    }

    /* For each subject, */
    _.forEach(subjects, (subject, folder) => {
        /* Read each folder */
        const files = fs.readdirSync(path.join(basepath, folder))
        /* Get all basenames that match reegxp as candidates for file existence */
        const candidates =
          _.uniq(files.filter(f => /^\d\d-\d\d-\d\d/.test(path.basename(f)))
                      .map(f => f.replace(/(_\d*)?\..*$/g, "")))
        /* For each candidate, */
        _.forEach(candidates, (candidate) => {
          /* Initialize the course object */
          const id = folder + "|" + candidate;
          /* Make the fancy date */
          const fancydate = candidate.replace(/(\d\d)-(\d\d)-(\d\d)/, "$3/$2/20$1")

          courses[id] = {
            date: candidate,
            coursename: folder,
            name: "Course " + fancydate,
            mainfile: "",
            sourcefile: "",
            altfile: null
          }

          /* Check if we're in the md case, or the latex file case */
          if (_.includes(files, candidate + ".md") &&
                (_.includes(files, candidate + ".md.html") ||
                 _.includes(files, candidate + ".html"))) {
              /* We're in the md case */
              courses[id].sourcefile = "md"
              courses[id].mainfile = _.includes(files, candidate + ".html") ? "html" : "md.html"
              /* Check if includes pdf */
              if (_.includes(files, candidate + ".pdf") ||
                  _.includes(files, candidate + ".md.pdf")) {
                /* It does. */
                courses[id].altfile = _.includes(files, candidate + ".pdf") ? "pdf" : "md.pdf"
              }
          } else if (_.includes(files, candidate + ".tex") &&
                     _.includes(files, candidate + ".pdf")) {
              /* We're in the latex case */
              courses[id].sourcefile = "tex"
              courses[id].mainfile = "pdf"
          }
        })
    })
    /* Connect to mysql db */
    let db = mysql.createConnection({
      host     : params.dbhost,
      user     : params.dbuser,
      password : params.dbpwd,
      database : params.dbname
    })
    db.connect()

    /* Upload subjects into database */
    Promise.all(_.map(subjects, (subject, namekey) =>
      /* Make a promise */
      new Promise((resolve, reject) => {
        /* Make queries */
        const queryupdate = `
          UPDATE subjects
          SET
            title = '${subject.title}',
            fulltitle = '${subject.fulltitle}',
            duration = ${subject.duration},
            shorttitle = '${subject.shorttitle}',
            link = '${subject.link}'
          WHERE courseid = '${subject.courseid}';`
        const queryinsert = `
          INSERT INTO
            subjects(title, fulltitle, duration, courseid, shorttitle, link)
          VALUES('${subject.title}', '${subject.fulltitle}', ${subject.duration}, '${subject.courseid}', '${subject.shorttitle}', '${subject.link}');`
        /* Query */
        db.query(queryupdate, (error, results, fields) => {
          /* If error, reject */
          if (error) { reject(error); return; }
          /* Check if result has been effective */
          if (results.affectedRows > 0) { resolve() }
          else {
            /* Start query insert */
            db.query(queryinsert, (error, results, fields) => {
              /* If error, reject */
              if (error) { reject(error) }
              else { resolve() }
            })
          }
        })
      })
    )).then(() =>
    /* Upload courses into database */
    Promise.all(_.map(courses, (course, namekey) =>
      /* Make a promise */
      new Promise((resolve, reject) => {
        /* Make queries */
        const altfile = course.altfile ? `'${course.altfile}'` : "NULL"
        const date = `STR_TO_DATE('${course.date}', '%y-%m-%d')`
        const queryupdate = `
          UPDATE courses
          SET
            name = '${course.name}',
            mainfile = '${course.mainfile}',
            sourcefile = '${course.sourcefile}',
            altfile = ${altfile}
          WHERE date = ${date}
          AND coursename = '${course.coursename}';`
        const queryinsert = `
          INSERT INTO
            courses(date, coursename, name, mainfile, sourcefile, altfile)
          VALUES(${date}, '${course.coursename}', '${course.name}', '${course.mainfile}', '${course.sourcefile}', ${altfile});`
        /* Query */
        db.query(queryupdate, (error, results, fields) => {
          /* If error, reject */
          if (error) { reject(error); return; }
          /* Check if result has been effective */
          if (results.affectedRows > 0) { resolve() }
          else {
            /* Start query insert */
            db.query(queryinsert, (error, results, fields) => {
              /* If error, reject */
              if (error) { reject(error) }
              else { resolve() }
            })
          }
        })
      })
    ))).catch(e => console.error(e))
  })
}
